# pyEdT python package

This package is under development for the moment!

[![pipeline status](https://gitlab.com/lucastorterotot/pyedt/badges/master/pipeline.svg)](https://gitlab.com/lucastorterotot/pyedt/pipelines)
[![coverage report](https://gitlab.com/lucastorterotot/pyedt/badges/master/coverage.svg)](https://gitlab.com/lucastorterotot/pyedt/-/jobs)
[![GitHub commit activity](https://img.shields.io/github/commit-activity/y/lucastorterotot/pyedt.svg)](https://gitlab.com/lucastorterotot/pyedt/commits)
[![GitHub last commit](https://img.shields.io/github/last-commit/lucastorterotot/pyedt.svg)](https://gitlab.com/lucastorterotot/pyedt/commits)
[![GitHub](https://img.shields.io/github/license/lucastorterotot/pyedt.svg)](https://gitlab.com/lucastorterotot/pyedt/blob/master/LICENSE)
[![Python Version](https://img.shields.io/badge/Python-3.6+-informational.svg)](https://www.python.org/)
[![matplotlib Version](https://img.shields.io/badge/matplotlib-3.1.0-informational.svg)](https://matplotlib.org/)

## Introduction

Ce module Python permet de gérer des emplois du temps d'élèves, d'enseignants et de salles de cours.

## Installation

1. Go in a directory where you want to install this repository and clone it:

- by `ssh` if you have a key:
```
git clone git@gitlab.com:lucastorterotot/pyedt.git --origin lucas
```

- else by `https`:
```
git clone https://gitlab.com/lucastorterotot/pyedt.git --origin lucas
```

2. Run the provided installation script:
```
./pyedt/install
```

## Usage

See the examples [in this directory](https://gitlab.com/lucastorterotot/pyedt/tree/master/examples).