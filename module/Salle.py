#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pyedt.module.utils import TimeTable

class Salle:
    def __init__(self,
                 name,
                ):
        self.name = name
        self._name = name.replace(" ", "_").replace(".", "_").replace("__", "_")
        self.table = TimeTable(name, mode = "salle")

    def add_slot(self,
                 matiere,
                 prof,
                 classe,
                 day,
                 start,
                 end = None,
                 duree = 1,
                ):
        if end is None :
            end = start + duree
        classe._add_slot(matiere, prof, self, day, start, end = end)
        prof._add_slot(matiere, classe, self, day, start, end = end)
        self._add_slot(
            matiere, prof, classe, day, start, end = end)

    def _add_slot(self,
                 matiere,
                 prof,
                 classe,
                 day,
                 start,
                 end = None,
                 duree = 1,
                ):
        if end is None :
            end = start + duree
        self.table.add_slot(
            matiere, prof, classe, self, day, start, end = end)

    def gen_table(self, name = "default",
                  start_fct = lambda x : x,
                  end_fct = lambda x : x,
                 ):
        if name == "default":
            name = "pyEdT-{}-{}".format("Salle", self._name)
        self.table.print(self.name, name, start_fct, end_fct)
