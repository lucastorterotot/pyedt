#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Matiere:
    def __init__(self,
                 name,
                 color = "blue2",
                ):
        self.name = name
        self._name = name.replace(" ", "_").replace(".", "_").replace("__", "_")
        self.color = color
