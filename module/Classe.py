#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pyedt.module.utils import TimeTable

class Classe:
    def __init__(self,
                 name,
                ):
        self.name = name
        self._name = name.replace(" ", "_").replace(".", "_").replace("__", "_")
        self.table = TimeTable(name, mode = "classe")
        self.groupes = []
        self.eleves = []

    def add_groupe(self,
                  groupe
                 ):
        if not groupe in self.groupes:
            self.groupes.append(groupe)
            for eleve in groupe.eleves:
                self.add_eleve(eleve)
        
    def add_eleve(self,
                  eleve
                 ):
        if not eleve in self.eleve:
            self.eleves.append(eleve)

    def add_slot(self,
                 matiere,
                 prof,
                 salle,
                 day,
                 start,
                 end = None,
                 duree = 1,
                ):
        if end is None :
            end = start + duree
        prof._add_slot(matiere, self, salle, day, start, end = end)
        salle._add_slot(matiere, prof, self, day, start, end = end)
        self._add_slot(matiere, prof, salle, day, start, end = end)

    def _add_slot(self,
                  matiere,
                  prof,
                  salle,
                  day,
                  start,
                  end = None,
                  duree = 1,
                  week = None,
                  source = "self",
                 ):
        if source == "self":
            source = self
        if end is None :
            end = start + duree
        done_eleves = []
        if source == self:
            for groupe in self.groupes:
                groupe._add_slot(
                    matiere, prof, salle, day, start, end = end, source = self)
                done_eleves += groupe.eleves
            for eleve in self.eleves:
                if not eleve in done_eleves:
                    eleve._add_slot(
                        matiere, prof, salle, day, start, end = end, source = self)
        self.table.add_slot(
            matiere, prof, self, salle, day, start, end = end, week = week)

    def gen_table(self, name = "default",
                  start_fct = lambda x : x,
                  end_fct = lambda x : x,
                 ):
        if name == "default":
            name = "pyEdT-{}-{}".format("Classe", self._name)
        self.table.print(self.name, name, start_fct, end_fct)

class Groupe:
    def __init__(self,
                 name,
                 classe,
                 week = "right",
                ):
        self.name = name
        self._name = name.replace(" ", "_").replace(".", "_").replace("__", "_")
        self.table = TimeTable(name, mode = "groupe")
        self.groupes = []
        self.classe = classe
        self.eleves = []
        self.classe.add_groupe(self)
        self.week = week

    def add_groupe(self,
                  groupe
                 ):
        if not groupe in self.groupes:
            self.groupes.append(groupe)
            for eleve in groupe.eleves:
                self.add_eleve(eleve)
        
    def add_eleve(self,
                  eleve
                 ):
        if not eleve in self.eleve:
            self.eleves.append(eleve)
        self.classe.add_eleve(eleve)

    def add_slot(self,
                 matiere,
                 prof,
                 salle,
                 day,
                 start,
                 end = None,
                 duree = 1,
                ):
        if end is None :
            end = start + duree
        prof._add_slot(matiere, self, salle, day, start, end = end)
        salle._add_slot(matiere, prof, self, day, start, end = end)
        self._add_slot(matiere, prof, salle, day, start, end = end)

    def _add_slot(self,
                  matiere,
                  prof,
                  salle,
                  day,
                  start,
                  end = None,
                  duree = 1,
                  source = "self",
                 ):
        if source == "self":
            source = self
        if end is None :
            end = start + duree
        done_eleves = []
        if source in [self, self.classe]:
            for groupe in self.groupes:
                groupe._add_slot(
                    matiere, prof,  salle, day, start, end = end, source = self)
                done_eleves += groupe.eleves
            for eleve in self.eleves:
                if not eleve in done_eleves:
                    eleve._add_slot(
                        matiere, prof, salle, day, start, end = end, source = self)
        if source != self.classe:
            self.classe._add_slot(matiere, prof, salle, day, start, end = end, week = "rect{}".format(self.week), source = self)
        self.table.add_slot(
            matiere, prof, self, salle, day, start, end = end)

    def gen_table(self, name = "default",
                  start_fct = lambda x : x,
                  end_fct = lambda x : x,
                 ):
        if name == "default":
            name = "pyEdT-{}-{}".format("Groupe", self._name)
        self.table.print(self.name, name, start_fct, end_fct)

class Eleve:
    def __init__(self,
                 name,
                 classe,
                 groupes = []
                ):
        self.name = name
        self._name = name.replace(" ", "_").replace(".", "_").replace("__", "_")
        self.table = TimeTable(name, mode = "eleve")
        self.classe = classe
        self.classe.add_eleve(self)
        self.groupes = groupes
        for groupe in self.groupes:
            groupe.add_eleve(self)

    def add_slot(self,
                 matiere,
                 prof,
                 salle,
                 day,
                 start,
                 end = None,
                 duree = 1,
                ):
        if end is None :
            end = start + duree
        prof._add_slot(matiere, self, salle, day, start, end = end)
        salle._add_slot(matiere, prof, self, day, start, end = end)
        self._add_slot(matiere, prof, salle, day, start, end = end)

    def _add_slot(self,
                  matiere,
                  prof,
                  salle,
                  day,
                  start,
                  end = None,
                  duree = 1,
                  source = "self",
                 ):
        if end is None :
            end = start + duree
        self.table.add_slot(
            matiere, prof, self, salle, day, start, end = end)

    def gen_table(self, name = "default",
                  start_fct = lambda x : x,
                  end_fct = lambda x : x,
                 ):
        if name == "default":
            name = "pyEdT-{}-{}".format("Eleve", self._name)
        self.table.print(self.name, name, start_fct, end_fct)
