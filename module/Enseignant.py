#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pyedt.module.utils import TimeTable
from pyedt.module.Classe import Classe, Groupe, Eleve
from pyedt.module.Salle import Salle

class Enseignant:
    def __init__(self,
                 name,
                ):
        self.name = name
        self._name = name.replace(" ", "_").replace(".", "_").replace("__", "_")
        self.table = TimeTable(name, mode = "enseignant")

    def add_slot(self,
                 matiere,
                 classe,
                 salle,
                 day,
                 start,
                 end = None,
                 duree = 1,
                ):
        if end is None :
            end = start + duree
        classe._add_slot(matiere, self, salle, day, start, end = end)
        salle._add_slot(matiere, self, classe, day, start, end = end)
        self._add_slot(
            matiere, classe, salle, day, start, end = end)

    def _add_slot(self,
                 matiere,
                 classe,
                 salle,
                 day,
                 start,
                 end = None,
                 duree = 1,
                ):
        if end is None :
            end = start + duree
        self.table.add_slot(
            matiere, self, classe, salle, day, start, end = end)

    def gen_table(self, name = "default",
                  start_fct = lambda x : x,
                  end_fct = lambda x : x,
                 ):
        if name == "default":
            name = "pyEdT-{}-{}".format("Enseignant", self._name)
        self.table.print(self.name, name, start_fct, end_fct)
