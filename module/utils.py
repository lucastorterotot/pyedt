#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import os

class Creneau:
    def __init__(self,
                 matiere,
                 prof,
                 classe,
                 salle,
                 day,
                 start, end,
                 color = "blue2",
                 week = None,
                 mode = "all",
                 start_fct = lambda x : x,
                 end_fct = lambda x : x,
                ):
        self.day = day
        self.start = start
        self.end = end
        self.color = color
        self.week = week
        self.matiere = matiere
        self.classe = classe
        self.prof = prof
        self.salle = salle
        self.mode = mode
        self.start_fct = start_fct
        self.end_fct = end_fct
        
    def print(self, name):
        output_file = "{}.tex".format(name)
        os.system(
            "echo '' >> {}".format(output_file)
        )
        os.system(
            "echo '\\\\creneau[' >> {}".format(output_file)
        )
        os.system(
            "echo '{} = {},' >> {}".format(
                "day", self.day,
                output_file,
            )
        )
        os.system(
            "echo '{} = {},' >> {}".format(
                "start", self.start_fct(self.start),
                output_file,
            )
        )
        os.system(
            "echo '{} = {},' >> {}".format(
                "end", self.end_fct(self.end),
                output_file,
            )
        )
        os.system(
            "echo '{} = ltcolor{},' >> {}".format(
                "color", self.color,
                output_file,
            )
        )
        if self.week is not None:
            os.system(
                "echo '{} = {},' >> {}".format(
                    "week", self.week,
                    output_file,
                )
            )
        os.system(
            "echo \] >> {}".format(output_file)
        )

        infos = []
        infos.append(self.matiere.name)
        if self.mode != "enseignant" and self.week is None and (self.end - self.start > .75):
            infos.append(self.prof.name)
        if self.mode not in ["classe", "groupe", "eleve"]:
            infos.append(self.classe.name)
        if self.mode != "salle":
            infos.append(self.salle.name)
        text = "\\\\\\\\\\\\".join(infos)
        os.system(
            "echo {"+"{}".format(text)+"}"+">> {}".format(output_file)
        )

class TimeTable:
    def __init__(self,
                 title,
                 mode = "classe",
                 start_fct = lambda x : x,
                 end_fct = lambda x : x,
                ):
        self.title = title
        self.mode = mode
        self.start_fct = start_fct
        self.end_fct = end_fct
        self.slots = {}
        for day in range(1,6):#["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"]:
            self.slots[day] = {}
            for slot in range(8,20):
                self.slots[day][slot] = 0
        self.graphictable = []

    def add_slot(self,
                 matiere,
                 prof,
                 classe,
                 salle,
                 day,
                 start,
                 end = None,
                 duree = 1,
                 week = None,
                ):
        if end is None :
            end = start + duree
        value = 1
        if week in ["rectleft", "rectright"]:
            value = .5
        # check conditions
        slots = range(int(start), int(end))
        for slot in slots :
            if self.slots[day][slot] >= 1 and not (self.mode == "salle" and self.title[:2] == "sg") and not (self.mode == "enseignant" and self.title == " - ") and not (self.mode == "salle" and self.title == " - ") and not (self.mode == "classe" and self.title == " - "):
                print(self.title, self.mode, day, slot)
                import pdb; pdb.set_trace()
            self.slots[day][slot] += value
        # build graphic table
        self.graphictable.append(
            Creneau(
                matiere,
                prof,
                classe,
                salle,
                day,
                start, end,
                color = matiere.color,
                week = week,
                mode = self.mode,
                start_fct = self.start_fct,
                end_fct = self.end_fct,
            )
        )

    def find_place(
        self,
        duree = 2,
        exclude = [],
        start = 8,
        end = 18,
        days = [1, 2, 3, 4, 5],
    ):
        result = []
        days = [d for d in days if d in self.slots]
        days = list(set(days))
        days.sort()
        for d in days:
            slots = [s for s in self.slots[d].keys()]
            slots = [s for s in slots if s >= start]
            slots = [s for s in slots if s <= end - duree]
            slots.sort()
            for s in slots:
                possible = True
                _duree = 0
                while _duree < duree and possible and _duree < 10:
                    if self.slots[d][s + _duree] != 0:
                        possible = False
                    if s + _duree in exclude:
                        possible = False
                    _duree += 1
                if possible:
                    result.append([d, s])
        return result

    def print(self, title, name, start_fct = None, end_fct = None):
        if start_fct is not None:
            self.start_fct = start_fct
        if end_fct is not None:
            self.end_fct = end_fct
        output_file = "{}.tex".format(name)
        os.system("echo '' > {}".format(output_file))
        os.system("echo '\\\\subsubsection*{"+"{}".format(title)+"}' >> "+"{}".format(output_file))
        os.system(
            "echo '\\\\jours{Lundi,Mardi,Mercredi,Jeudi,Vendredi}' >> "+"{}".format(output_file)
        )
        os.system(
            "echo '\\\\begin{tikzpicture}[yscale=0.525,every node/.style={yscale=0.5,xscale=1}]' >> "+"{}".format(output_file)
        )
        os.system(
            "echo '\\\\tikzstyle{every node}=[font=\\\\footnotesize]' >> "+"{}".format(output_file)
        )
        os.system(
            "echo '\\\\planning[demiheures,quartheures,end=18]' >> "+"{}".format(output_file)
        )
        for creneau in self.graphictable:
            creneau.start_fct = self.start_fct
            creneau.end_fct = self.end_fct
            creneau.print(name)
        os.system(
            "echo '\\\\end{tikzpicture}' >> "+"{}".format(output_file)
        )
